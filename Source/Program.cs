﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Source
{
    
    class Computer
    {
        public OS OS { get; set; }
        public void Launch(string osName)
        {
            OS = OS.getInstance(osName);
        }
    }
    class Notebook:Computer
    {
        public OS newOS { get; set; }
        public void newLaunch(string osName)
        {
            newOS = OS.getInstance(osName);
        }        
    }
    //Паттерн - одиночка
    class OS
    {
        private static OS instance; // Сюда будет записываться единственный объект класса

        public string Name { get; private set; } // Имя, чтобы было что вывести

        protected OS(string name) //В оригинале было private, но взломать систему через наследование не удалось
        {
            this.Name = name;//
        }

        public static OS getInstance(string name)
        {
            if (instance == null)
                instance = new OS(name);
            return instance;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Computer comp = new Computer();
            comp.Launch("Windows 8.1");
            Console.WriteLine(comp.OS.Name);
            //Запустить ноут под FreeBSD не удасться, так как у нас есть единственный вариант ОС - WIndows
            Notebook note = new Notebook();
            note.newLaunch("FreeBSD");
            Console.WriteLine(note.newOS.Name);
            // у нас не получится изменить ОС, так как объект уже создан    
            comp.OS = OS.getInstance("Windows 10");
            Console.WriteLine(comp.OS.Name);

            Console.ReadLine();
        }
    }
}
